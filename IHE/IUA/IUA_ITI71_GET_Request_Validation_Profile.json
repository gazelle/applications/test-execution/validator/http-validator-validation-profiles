{
  "profileType": "HTTPREQUEST",
  "id": "IUA_ITI71_GET_Request_Validation_Profile",
  "name": "ITI-71_HttpGET_Request",
  "description": "Validation Profile for validating an ITI-71 Get HTTP Request",
  "context": "IHE",
  "assertions": [
    {
      "selector": "request.method",
      "id": "ITI71-008_GETMethodChecking",
      "description": "The HTTP method shall be GET",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "FIXEDVALUE",
          "fixedValue": "GET"
        }
      ]
    },
    {
      "selector": "request.version",
      "id": "ITI71-008_HTTP1VersionChecking",
      "description": "The HTTP version shall be HTTP/1.1 ",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "FIXEDVALUE",
          "fixedValue": "HTTP/1.1"
        }
      ]
    },
    {
      "selector": "request.uri.path",
      "id": "ITI71-008_URIRegexChecking",
      "description": "Assertion for the HTTP uri checking",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(\\/[^\\/\\s]+)+$"
        }
      ]
    },
    {
      "selector": "request.headers('Host').values",
      "id": "ITI71-008_HostChecking",
      "description": "Host Shall be a server name; Regex: ^(http[s]?:\\/\\/)?[a-zA-Z0-9.\\/]+$",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(http[s]?:\\/\\/)?[a-zA-Z0-9.\\/]+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('response_type').values",
      "id": "ITI71-008_response_type_parameter_presence",
      "description": "One and only one response_type parameter is required.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 1,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('response_type').values",
      "id": "ITI71-008_response_type_parameter_value",
      "description": "response_type parameter's value SHALL be code.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "FIXEDVALUE",
          "fixedValue": "code"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('client_id').values",
      "id": "ITI71-008_client_id_parameter_presence",
      "description": "One and only one client_id is required.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 1,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('client_id').values",
      "id": "ITI71-008_client_id_parameter_value",
      "description": "client_id parameter's value SHALL be a b64token; regex: [-a-zA-Z0-9._~+\/]+=*.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "[-a-zA-Z0-9._~+\\/]+=*"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('state').values",
      "id": "ITI71-008_state_parameter_presence",
      "description": "One and only one state parameter is required in the HTTP GET request.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 1,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('state').values",
      "id": "ITI71-008_state_parameter_value",
      "description": "State parameter's value SHALL be a random unguessable value; regex: [-a-zA-Z0-9._~+/]+=*.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "[-a-zA-Z0-9._~+\\/]+=*"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('code_challenge').values",
      "id": "ITI71-008_code_challenge_parameter_presence",
      "description": "One and only one code_challenge parameter is required in the HTTP GET request.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 1,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('code_challenge').values",
      "id": "ITI71-008_code_challenge_parameter_value",
      "description": "code_challenge parameter's value SHALL be b64 value; regex: [-a-zA-Z0-9._~+/]+=*.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "[-a-zA-Z0-9._~+\\/]+=*"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('resource').values",
      "id": "ITI71-009_resource_parameter_presence",
      "description": "One resource parameter is permitted in the HTTP GET request.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('resource').values",
      "id": "ITI71-009_resource_parameter_regex",
      "description": "Resource parameter's value SHALL be a URL. Regex: ^http[s]?:\/\/[a-zA-Z0-9.\/]*$",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('resource').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^http[s]?:\\/\\/[a-zA-Z0-9.\\/]*$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('code_challenge_method').values",
      "id": "ITI71-009_code_challenge_method_parameter_presence",
      "description": "One code_challenge_method parameter is permitted in the HTTP GET request.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('code_challenge_method').values",
      "id": "ITI71-009_code_challenge_method_parameter_value",
      "description": "code_challenge_method parameter's value may be S256",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('code_challenge_method').values.size() > 0",
      "checks": [
        {
          "type": "FIXEDVALUE",
          "fixedValue": "S256"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('redirect_uri').values",
      "id": "ITI71-009_redirect_uri_parameter_presence",
      "description": "One redirect_uri parameter is permitted in the HTTP GET request.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('redirect_uri').values",
      "id": "ITI71-009_redirect_uri_method_parameter_value",
      "description": "redirect_uri parameter's value SHALL be a URI. Regex: http[s]?:\\/\\/[a-zA-Z0-9.\\/]*",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('redirect_uri').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "http[s]?:\\/\\/[a-zA-Z0-9.\\/]*"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('scope').values",
      "id": "ITI71-009_scope_parameter_presence",
      "description": "One scope parameter is permitted in the HTTP GET request.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('scope').values",
      "id": "ITI71-009_scope_parameter_value",
      "description": "Scope parameter's value SHALL match the rules : scope = scope-token *( SP scope-token ) AND scope-token = 1*( %x21 / %x23-5B / %x5D-7E ); regex: ([\\!|\\#-\\[|\\]-\\~]+| )+",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('scope').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "([\\!|\\#-\\[|\\]-\\~]+| )+"
        }
      ]
    }
  ]
}