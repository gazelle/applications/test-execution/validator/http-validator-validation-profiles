---
title: CH:mCSD HTTP Validation profiles' assertions 
subtitle: List of all HTTP rules for CH:mCSD
authors: Vincent HOFMAN
date: 2023-03-14
---

# CH:mCSD HTTP Validation profiles' assertions

## Common assertions for all resources

|   id  |   Check's type |   Value |   Description    |
|---    |---    |---    |---    |
|   GETMethodChecking   |   CLOSEDLIST |   [GET,POST] |   HTTP method shall be GET  |
|   HTTP1VersionChecking   |   FIXEDVALUE  |   HTTP/1.1 |   The HTTP version shall be HTTP/1.1  |
|   URIRegexChecking   |   REGEX   |   `^(\S)+\/FHIR-Resource$` |   URI path of the request shall be followed by the mCSD resource  |
|   HostChecking   |   REGEX   |   `^(http[s]?:\/\/)?[a-zA-Z0-9.\-\/]+(\:(\d)+)?$` |   Host Shall be a server name  |
|   Authorization_parameter_presence   |   OCCURRENCE   |   1..1 |   The Authorization parameter is mandatory  |
|   Authorization_parameter_value   |   REGEX   |   `^Bearer\\ [-a-zA-Z0-9._~+\\/]+=*$` |   Authorization parameter's value SHALL be Bearer, followed by the token.  |
|   traceparent_parameter_presence   |   OCCURRENCE   |   0..1 |   The traceparent parameter can be used only once.|
|   traceparent_parameter_value   |   REGEX   |   `^(?!f{2})([\da-f]{2})-(?!0{32})([\da-f]{32})-(?!0{16})([\da-f]{16})-([\da-f]{2})$` |   traceparent parameter's value SHALL be as defined in fhir.ch, section [tracecontext](https://fhir.ch/ig/ch-epr-mhealth/tracecontext.html). |
|   _id_parameter_presence   |   OCCURENCE   | 0..1 |   _id parameter can be used only once. |
|   _id_parameter_value   |   REGEX   |   `^\S+$` |   _id parameter is a token.  |
|   _lastUpdated_parameter_presence   |   OCCURENCE   | 0..1 |   _lastUpdated parameter can be used only once. |
|   _lastUpdated_parameter_value   |   REGEX   |   `^(ge\|le)?[0-9]{4}-(0[1-9]\|1[0-2])-(0[1-9]\|1\d\|2[0-9]\|3[01])$` | _lastUpdated parameter is made of a date eventually preceded by a modifier ge or le.    |
|   _format_parameter_presence   |   OCCURENCE   | 0..1 |   _format parameter can be used only once. |
|   _format_parameter_value   |   CLOSEDLIST   |   [json,xml] |   _format parameter's value shall be either json or xml.
|   _sort_parameter_presence   |   OCCURENCE   | 0..1 |   _sort parameter can be used only once. |
|   _sort_parameter_value   |   REGEX   |   `^([\w,]\|(-\w))+$` |   _sort parameter's value is made of items composed of word caracters, separated by a comma and eventually prefixed by a dash. | 
|   _count_parameter_presence   |   OCCURENCE   | 0..1 |   _count parameter can be used only once. |
|   _count_parameter_value   |   REGEX   |   `^\d+$` |   _count parameter is a decimal.  |
|   active_parameter_presence   |   OCCURENCE   | 0..1 |   active parameter can be used only once. |
|   active_parameter_value   |   CLOSEDLIST   |   [true,false] |   active parameter is a boolean and accept true or false values.  |

## Organization resource's assertions

|   id  |   Check's type |   Value |   Description    |
|---    |---    |---    |---    |
|   identifier_parameter_presence   |   OCCURENCE   | 0..1 |   identifier parameter can be used only once. |
|   identifier_parameter_value   |   REGEX   |   `^\S+$` |   identifier parameter is a token.  |
|   name_parameter_presence   |   OCCURENCE   | 0..1 |   name parameter can be used only once. |
|   name_parameter_value   |   REGEX   |   `^.+$` |   name parameter is a string.  |
|   partof_parameter_presence   |   OCCURENCE   | 0..1 |   partof parameter can be used only once. |
|   partof_parameter_value   |   REGEX   |   `^\S+$` |  partof parameter is a token.   |
|   type_parameter_presence   |   OCCURENCE   | 0..1 |   type parameter can be used only once. |
|   type_parameter_value |   REGEX   |   `^.+$` |  type parameter is a token.   |

## Practitioner resource's assertions

|   id  |   Check's type |   Value |   Description    |
|---    |---    |---    |---    | 
|   identifier_parameter_presence   |   OCCURENCE   | 0..1 |   identifier parameter can be used only once. |
|   identifier_parameter_value   |   REGEX   |   `^\S+$` |   identifier parameter is a token.  |
|   name_parameter_presence   |   OCCURENCE   | 0..1 |   name parameter can be used only once, eventually suffixed by a modifier :contains or :exact.
|   name_parameter_value   |   REGEX   |  `^.+$`   | name parameter is made of a string.    |
|   given_parameter_presence   |   OCCURENCE   | 0..1 |   given parameter can be used only once, eventually suffixed by a modifier :contains or :exact.
|   given_parameter_value   |   REGEX   |  `^.+$`   | given parameter is made of a string.    |
|   family_parameter_presence   |   OCCURENCE   | 0..1 |   family parameter can be used only once, eventually suffixed by a modifier :contains or :exact.
|   family_parameter_value   |   REGEX   |  `^.+$`   | family parameter is made of a string.    |

## PractitionerRole resource's assertions

|   id  |   Check's type |   Value |   Description    |
|---    |---    |---    |---    |
|   organization_parameter_presence   | OCCURENCE   | 0..1 |   organization parameter can be used only once. |
|   organization_parameter_value   |   REGEX   |   `^\S+$` |  organization parameter is a token.   |
|   practitioner_parameter_presence   | OCCURENCE   | 0..1 |   practitioner parameter can be used only once. |
|   practitioner_parameter_value   | REGEX |  `^\S+$` |  practitioner parameter is a token.
|   role_parameter_presence   | OCCURENCE   | 0..1 |   role parameter can be used only once. |
|   role_parameter_value   |   REGEX |  `^\S+$` |  role parameter is a token.
|   service_parameter_presence   | OCCURENCE   | 0..1 |   service parameter can be used only once. |
|   service_parameter_value   |   REGEX |  `^\S+$` |  service parameter is a token.
|   speciality_parameter_presence   | OCCURENCE   | 0..1 |   speciality parameter can be used only once. |
|   speciality_parameter_value   |   REGEX |  `^\S+$` |  speciality parameter is a token.|
|   _include__parameter_presence   | OCCURENCE   | 0..1 |   _include parameter can be used only once. |
|   _include_parameter_value   |   REGEX |  `^.+$` |  _include parameter is a string.|

## Behaviour of simulators using this HTTP Validation profile
In case of error response from HTTP Validator, simulators shall sent these HTTP error codes :
|   Assertion's id  |   HTTP error code  |
|---    |---    |
|   GETMethodChecking   | 404 |
|   Authorization_parameter_presence   | 401 |
|   Authorization_parameter_presence   | 401 |
|   All other assertions (default)   | 400 |