{
  "profileType": "HTTPREQUEST",
  "id": "CH_ITI-90_Practitioner_GET_mCSD_Query",
  "name": "CH_ITI-90_Practitioner_HTTP_GET_Request",
  "description": "Validation Profile for validating a Get mCSD Query.",
  "context": "EPR",
  "assertions": [
    {
      "selector": "request.method",
      "id": "GETMethodChecking",
      "description": "HTTP method shall be GET or POST",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "CLOSEDLIST",
          "values": [
            "GET",
            "POST"
          ]
        }
      ]
    },
    {
      "selector": "request.version",
      "id": "HTTP1VersionChecking",
      "description": "The HTTP version shall be HTTP/1.1",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "FIXEDVALUE",
          "fixedValue": "HTTP/1.1"
        }
      ]
    },
    {
      "selector": "request.uri.path",
      "id": "URIRegexChecking",
      "description": "URI path of the request shall be followed by the mCSD resource",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(\\S)+Practitioner(\\/_search)?$"
        }
      ]
    },
    {
      "selector": "request.headers('Host').values",
      "id": "HostChecking",
      "description": "Host Shall be a server name; Regex: ^(http[s]?:\\/\\/)?[a-zA-Z0-9.\\-\\/]+(:(\\d)+)?$",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(http[s]?:\\/\\/)?[a-zA-Z0-9.\\-\\/]+(:(\\d)+)?$"
        }
      ]
    },
    {
      "selector": "request.headers('Authorization').values",
      "id": "Authorization_parameter_presence",
      "description": "The Authorization parameter is mandatory.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 1,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.headers('Authorization').values",
      "id": "Authorization_parameter_value",
      "description": "Authorization parameter's value SHALL be Bearer, followed by the token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^Bearer\\ [-a-zA-Z0-9._~+\\/]+=*$"
        }
      ]
    },
    {
      "selector": "request.headers('traceparent').values",
      "id": "traceparent_parameter_presence",
      "description": "The traceparent parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.headers('traceparent').values",
      "id": "traceparent_parameter_value",
      "description": "traceparent parameter's value SHALL be as defined in fhir.ch, section tracecontext.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",      
      "applyIf": "request.headers('traceparent').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(?!f{2})([\\da-f]{2})-(?!0{32})([\\da-f]{32})-(?!0{16})([\\da-f]{16})-([\\da-f]{2})$"
        }
      ]
    },{
      "selector": "request.headers('Accept').values",
      "id": "Accept_parameter_presence",
      "description": "The Accept parameter is mandatory.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 1,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.headers('Accept').values",
      "id": "Accept_parameter_value",
      "description": "Accept parameter's value SHALL be as defined in https://build.fhir.org/http.html#mime-type.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^application\\/fhir\\+(xml|json|turtle)(;( )?fhirVersion=\\d\\.\\d)?$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('_id').values",
      "id": "_id_parameter_presence",
      "description": "_id parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('_id').values",
      "id": "_id_parameter_value",
      "description": "_id parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('_id').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('_lastUpdated').values",
      "id": "_lastUpdated_parameter_presence",
      "description": "_lastUpdated parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('_lastUpdated').values",
      "id": "_lastUpdated_parameter_value",
      "description": "_lastUpdated parameter is made of a date eventually preceded by a modifier ge or le.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('_lastUpdated').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(ge|le)?[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|1\\d|2[0-9]|3[01])$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('_format').values",
      "id": "_format_parameter_presence",
      "description": "_format parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('_format').values",
      "id": "_format_parameter_value",
      "description": "_format parameter's value shall be either json or xml.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('_format').values.size() > 0",
      "checks": [
        {
          "type": "CLOSEDLIST",
          "values": [
            "json",
            "xml"
          ]
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('_sort').values",
      "id": "_sort_parameter_presence",
      "description": "_sort parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('_sort').values",
      "id": "_sort_parameter_value",
      "description": "_sort parameter's value is made of items composed of word caracters, separated by a comma and eventually prefixed by a dash.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('_sort').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^([\\w,]|(-\\w))+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('_count').values",
      "id": "_count_parameter_presence",
      "description": "_count parameter can be used only once..",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('_count').values",
      "id": "_count_parameter_value",
      "description": "_count parameter is a decimal.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('_count').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "\\d+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('active').values",
      "id": "active_parameter_presence",
      "description": "active parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('active').values",
      "id": "active_parameter_value",
      "description": "active parameter is a boolean and accept true or false values.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('active').values.size() > 0",
      "checks": [
        {
          "type": "CLOSEDLIST",
          "values": [
            "true",
            "false"
          ]
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('identifier').values",
      "id": "identifier_parameter_presence",
      "description": "identifier parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('identifier').values",
      "id": "identifier_parameter_value",
      "description": "identifier parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('identifier').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('name').values",
      "id": "name_parameter_presence",
      "description": "name parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('name').values",
      "id": "name_parameter_value",
      "description": "name parameter is a string.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('name').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^.+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('given').values",
      "id": "given_parameter_presence",
      "description": "given parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('given').values",
      "id": "given_parameter_value",
      "description": "given parameter's value is made of items composed of string.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('given').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^[\\s\\S]+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('family').values",
      "id": "family_parameter_presence",
      "description": "family parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('family').values",
      "id": "family_parameter_value",
      "description": "family parameter's value is made of items composed of string.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('family').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^([\\w,]|(-\\w))+$"
        }
      ]
    }
  ]
}