---
title: CH:ATC HTTP Validation profiles' assertions 
subtitle: List of all HTTP rules for CH:ATC
authors: Vincent HOFMAN
date: 2025-01-06
---

# CH:ATC HTTP Validation profiles' assertions

|   id  |   Check's type |   Value |   Description    |
|---    |---    |---    |---    |
|   GETMethodChecking   |   FIXEDVALUE |   `GET` |   HTTP method shall be GET  |
|   HTTP1VersionChecking   |   FIXEDVALUE  |   `HTTP/1.1` |   The HTTP version shall be HTTP/1.1  |
|   URIRegexChecking   |   REGEX   |   `^(\S)+\/AuditEvent$` |   URI path of the request shall be followed by the mCSD resource  |
|   HostChecking   |   REGEX   |   `^(http[s]?:\/\/)?[a-zA-Z0-9.\-\/]+(\:(\d)+)?$` |   Host Shall be a server name  |
|   Authorization_parameter_presence   |   OCCURRENCE   |  `1..1` |   The Authorization parameter is mandatory  |
|   Authorization_parameter_value   |   REGEX   |   `^Bearer\\ [-a-zA-Z0-9._~+\\/]+=*$` |   Authorization parameter's value SHALL be Bearer, followed by the token.  |
|   traceparent_parameter_presence   |   OCCURRENCE   |   `0..1` |   The traceparent parameter can be used only once.|
|   traceparent_parameter_value   |   REGEX   |   `^(?!f{2})([\da-f]{2})-(?!0{32})([\da-f]{32})-(?!0{16})([\da-f]{16})-([\da-f]{2})$` |   traceparent parameter's value SHALL be as defined in fhir.ch, section [tracecontext](https://fhir.ch/ig/ch-epr-mhealth/tracecontext.html). |
|   date_parameter_presence   |   REGEX   | `^(?=.*date=).+$` |   At least one date parameter shall be present; Two date parameters are recommended. |
|   date_parameter_value   |   REGEX   |   `^(eq\|ne\|lt\|gt\|ge\|le\|sa\|eb\|ap)\\d{4}-\\d{2}-\\d{2}$` |   date parameter's value is composed by a comparator and YYYY-MM-DD. regex: '^(eq\|ne\|lt\|gt\|ge\|le\|sa\|eb\|ap)\\d{4}-\\d{2}-\\d{2}$'.  |
|   address_parameter_presence   |   OCCURENCE   | `0..0` |   address parameter shall not be used. |
|   agent.identifier_parameter_value   |   REGEX   | `^.*?$`   |   agent.identifier parameter's value shall be a token. |
|   patient.identifier_parameter_presence   |   OCCURENCE   | `0..0` |   patient.identifier parameter shall not be used. |
|   entity.identifier_parameter_presence   |   OCCURENCE   | `1..1` |   One and only one entity.identifier parameter is required. |
|   entity.identifier_parameter_value   |   REGEX   |   `^urn\\:oid\\:2\\.16\\.756\\.5\\.30\\.1\\.127\\.3\\.10\\.3\\|\\d+$` |   entity.identifier parameter's value is a token composed of the Assigning Authority urn:oid:2.16.756.5.30.1.127.3.10.3, a pipe and the EPR-SPID.  |
|   entity-type_parameter_value   |   REGEX   |   `^.*?$` |   entity-type parameter's value shall be a token.  |
|   entity-role_parameter_value   |   REGEX   |   `^.*?$` |   entity-role parameter's value shall be a token.  |
|   source_parameter_value   |OCCURENCE   | `0..0` |   source parameter shall not be used. |
|   type_parameter_value   |OCCURENCE   | `0..0` |   type parameter shall not be used. |
|   subtype_parameter_value   |   REGEX   |   `^.*?$` |   subtype parameter's value shall be a token.  |
|   outcome_parameter_value   |OCCURENCE   | `0..0` |   outcome parameter shall not be used. |


## Behaviour of simulators using this HTTP Validation profile
In case of error response from HTTP Validator, simulators shall send these HTTP error codes :
|   Assertion's id  |   HTTP error code  |
|---    |---    |
|   GETMethodChecking   | 404 |
|   Authorization_parameter_presence   | 401 |
|   Authorization_parameter_presence   | 401 |
|   All other assertions (default)   | 400 |