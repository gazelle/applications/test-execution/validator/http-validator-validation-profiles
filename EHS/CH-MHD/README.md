---
title: CH:MHD HTTP Validation profiles' assertions 
subtitle: List of all HTTP rules for CH:MHD/ITI-65/ITI-66/ITI-67/ITI-68
authors: Vincent HOFMAN
date: 2024-05-30
---

# CH:MHD Mobile Access to Health Documents validation profiles

## References
These validation profiles' documentation reference:
- [FHIR's Datatypes](https://build.fhir.org/datatypes.html)
- [String type's modifiers](https://build.fhir.org/search.html#modifiers)

## ITI-65 Provide Document Bundle assertions

|   id  |   Check's type |   Value |   Description    |
|---    |---    |---    |---    |
|   MethodChecking   |   FIXEDVALUE   |   POST |   HTTP method shall be POST  |
|   HTTP1VersionChecking   |   FIXEDVALUE  |   HTTP/1.1 |   The HTTP version shall be HTTP/1.1  |
|   URIRegexChecking   |   REGEX   |   `^(\S)+$` |   URI path of the request shall be made of non space caracters  |
|   HostChecking   |   REGEX   |   `^(http[s]?:\/\/)?[a-zA-Z0-9.\-\/]+(\:(\d)+)?$` |   Host Shall be a server name  |
|   Authorization_parameter_presence   |   OCCURRENCE   |   1..1 |   The Authorization parameter is mandatory  |
|   Authorization_parameter_value   |   REGEX   |   `^Bearer\\ [-a-zA-Z0-9._~+\\/]+=*$` |   Authorization parameter's value SHALL be Bearer, followed by the token.  |
|   traceparent_parameter_presence   |   OCCURRENCE   |   0..1 |   The traceparent parameter can be used only once.|
|   traceparent_parameter_value   |   REGEX   |   `^(?!f{2})([\da-f]{2})-(?!0{32})([\da-f]{32})-(?!0{16})([\da-f]{16})-([\da-f]{2})$` |   traceparent parameter's value SHALL be as defined in fhir.ch, section [tracecontext](https://fhir.ch/ig/ch-epr-mhealth/tracecontext.html). |
|   _format_parameter_presence   |   OCCURRENCE   |   0..1 |   _format parameter can be used only once. |
|   _format_parameter_value   |   CLOSEDLIST   |   [json,xml] |   _format parameter's value shall be either json or xml. |


## ITI-66 Find Document Lists assertions

|   id  |   Check's type |   Value |   Description    |
|---    |---    |---    |---    |
|   MethodChecking   |   CLOSEDLIST   |   [GET,POST] |   HTTP method shall be either GET or POST  |
|   HTTP1VersionChecking   |   FIXEDVALUE  |   HTTP/1.1 |   The HTTP version shall be HTTP/1.1  |
|   URIRegexChecking   |   REGEX   |   `^(\S)+List$` |   URI path of the request shall be followed by the MHD resource  |
|   HostChecking   |   REGEX   |   `^(http[s]?:\/\/)?[a-zA-Z0-9.\-\/]+(\:(\d)+)?$` |   Host Shall be a server name  |
|   Authorization_parameter_presence   |   OCCURRENCE   |   1..1 |   The Authorization parameter is mandatory  |
|   Authorization_parameter_value   |   REGEX   |   `^Bearer\\ [-a-zA-Z0-9._~+\\/]+=*$` |   Authorization parameter's value SHALL be Bearer, followed by the token.  |
|   traceparent_parameter_presence   |   OCCURRENCE   |   0..1 |   The traceparent parameter can be used only once.|
| traceparent_parameter_value   |   REGEX   |   `^(?!f{2})([\da-f]{2})-(?!0{32})([\da-f]{32})-(?!0{16})([\da-f]{16})-([\da-f]{2})$` |   traceparent parameter's value SHALL be as defined in fhir.ch, section [tracecontext](https://fhir.ch/ig/ch-epr-mhealth/tracecontext.html). |
|   code_parameter_presence   |   OCCURRENCE   |   1..1 |   The Document Consumer shall include search parameter code. |
|   code_parameter_value   |   REGEX   |   `^\S+$` |   code parameter is a token.  |
|   date_parameter_presence   |   OCCURENCE   | 0..1 |   date parameter can be used only once. |
|   date_parameter_value   |   REGEX   |   `^(eq\|ne\|lt\|gt\|ge\|le\|sa\|eb\|ap)?[0-9]{4}-(0[1-9]\|1[0-2])-(0[1-9]\|1\d\|2[0-8]\|3[01])$` | date parameter is made of a date eventually preceded by a modifier (eq,ne,lt,gt,ge,le,sa,eb or ap).    |
|   designationType_parameter_presence   |   OCCURRENCE   |   0..1 |   designationType parameter can be used only once. |
|   designationType_parameter_value   |   REGEX   |   `^\S+$` |   designationType parameter is a token.  |
|   identifier_parameter_presence   |   OCCURRENCE   |   0..1 |   identifier parameter can be used only once. |
|   identifier_parameter_value   |   REGEX   |   `^\S+$` |   identifier parameter is a token.  |
|   patient_or_patient.identifier_presence[^1].   |   REGEX   |   \bpatient(\.identifier)?\b |   The Document Consumer shall include search parameter patient or patient.identifier. |
|   patient_parameter_presence   |   OCCURRENCE   |   0..1 |   patient parameter can be used only once. |
|   patient_parameter_value   |   REGEX   |   `^\S+$` |   patient parameter is a token.  |
|   patient.identifier_parameter_presence   |   OCCURRENCE   |   0..1 |   patient.identifier parameter can be used only once. |
|   patient.identifier_parameter_value   |   REGEX   |   `^\S+$` |   patient.identifier parameter is a token.  |
|   source.given_parameter_presence   |   OCCURRENCE   |   0..1 |   source.given parameter can be used only once. |
|   source.given_parameter_value   |   REGEX   |  `^[\s\S]+$`   | source.given parameter is made of a string.    |
|   source.family_parameter_presence   |   OCCURRENCE   |   0..1 |   source.family parameter can be used only once. |
|   source.family_parameter_value   |   REGEX   |  `^[\s\S]+$`   | source.family parameter is made of a string.    |
|   sourceId_parameter_presence   |   OCCURRENCE   |   0..1 |   sourceId parameter can be used only once. |
|   sourceId_parameter_value   |   REGEX   |   `^\S+$` |   sourceId parameter is a token.  |
|   status_parameter_presence   |   OCCURRENCE   |   1..1 |   The Document Consumer shall include search status. |
|   status_parameter_value   |   REGEX   |   `^\S+$` |   status parameter is a token.  |

[^1]: Can't be check with current HTTP Validator; see issue: https://gazelle.ihe.net/jira/browse/HTTP-57

## ITI-67 Find Document References assertions

|   id  |   Check's type |   Value |   Description    |
|---    |---    |---    |---    |
|   MethodChecking   |   CLOSEDLIST   |   [GET,POST] |   HTTP method shall be either GET or POST  |
|   HTTP1VersionChecking   |   FIXEDVALUE  |   HTTP/1.1 |   The HTTP version shall be HTTP/1.1  |
|   URIRegexChecking   |   REGEX   |   `^(\S)+DocumentReference$` |   URI path of the request shall be followed by the MHD resource  |
|   HostChecking   |   REGEX   |   `^(http[s]?:\/\/)?[a-zA-Z0-9.\-\/]+(\:(\d)+)?$` |   Host Shall be a server name  |
|   Authorization_parameter_presence   |   OCCURRENCE   |   1..1 |   The Authorization parameter is mandatory  |
|   Authorization_parameter_value   |   REGEX   |   `^Bearer\\ [-a-zA-Z0-9._~+\\/]+=*$` |   Authorization parameter's value SHALL be Bearer, followed by the token.  |
|   traceparent_parameter_presence   |   OCCURRENCE   |   0..1 |   The traceparent parameter can be used only once.|
| traceparent_parameter_value   |   REGEX   |   `^(?!f{2})([\da-f]{2})-(?!0{32})([\da-f]{32})-(?!0{16})([\da-f]{16})-([\da-f]{2})$` | traceparent parameter's value SHALL be as defined in fhir.ch, section [tracecontext](https://fhir.ch/ig/ch-epr-mhealth/tracecontext.html). |
|   author.given_parameter_presence   |   OCCURRENCE   |   0..1 |   author.given parameter can be used only once. |
|   author.given_parameter_value   |   REGEX   |  `^[\s\S]+$`   | author.given parameter is made of a string. |
|   author.family_parameter_presence   |   OCCURRENCE   |   0..1 |   author.family parameter can be used only once. |
|   author.family_parameter_value   |   REGEX   |  `^[\s\S]+$`   | author.family parameter is made of a string.    |
|   category_parameter_presence   |   OCCURRENCE   |   0..1 |   category parameter can be used only once. |
|   category_parameter_value   |   REGEX   |   `^\S+$` |   category parameter is a token.  |
|   creation_parameter_presence   |   OCCURRENCE   |   0..1 |   creation parameter can be used only once. |
|   creation_parameter_value   |   REGEX   |   `^(eq\|ne\|lt\|gt\|ge\|le\|sa\|eb\|ap)?([0-9]([0-9]([0-9][1-9]\|[1-9]0)\|[1-9]00)\|[1-9]000)(-(0[1-9]\|1[0-2])(-(0[1-9]\|[1-2][0-9]\|3[0-1])(T([01][0-9]\|2[0-3]):[0-5][0-9]:([0-5][0-9]\|60)(\.[0-9]{1,9})?)?)?(Z\|(\+\|-)((0[0-9]\|1[0-3]):[0-5][0-9]\|14:00)?)?)?$` |   creation parameter is a dateTime eventually preceded by a modifier (eq,ne,lt,gt,ge,le,sa,eb or ap).  |
|   date_parameter_presence   |   OCCURENCE   | 0..1 |   date parameter can be used only once. |
|   date_parameter_value   |   REGEX   |   `^(eq\|ne\|lt\|gt\|ge\|le\|sa\|eb\|ap)?[0-9]{4}-(0[1-9]\|1[0-2])-(0[1-9]\|1\d\|2[0-8]\|3[01])$` | date parameter is made of a date eventually preceded by a modifier (eq,ne,lt,gt,ge,le,sa,eb or ap).    |
|   event_parameter_presence   |   OCCURRENCE   |   0..1 |   event parameter can be used only once. |
|   event_parameter_value   |   REGEX   |   `^\S+$` |   event parameter is a token.  |
|   facility_parameter_presence   |   OCCURRENCE   |   0..1 |   facility parameter can be used only once. |
|   facility_parameter_value   |   REGEX   |   `^\S+$` |   facility parameter is a token.  |
|   format_parameter_presence   |   OCCURRENCE   |   0..1 |   format parameter can be used only once. |
|   format_parameter_value   |   REGEX   |   `^\S+$` |   format parameter is a token.  |
|   identifier_parameter_presence   |   OCCURRENCE   |   0..1 |   identifier parameter can be used only once. |
|   identifier_parameter_value   |   REGEX   |   `^\S+$` |   identifier parameter is a token.  |
|   patient_or_patient.identifier_presence   |   REGEX   |   \bpatient(\.identifier)?\b |   The Document Consumer shall include search parameter patient or patient.identifier. |
|   patient_parameter_presence   |   OCCURRENCE   |   0..1 |   patient parameter can be used only once. |
|   patient_parameter_value   |   REGEX   |   `^\S+$` |   patient parameter is a token.  |
|   patient.identifier_parameter_presence   |   OCCURRENCE   |   0..1 |   patient.identifier parameter can be used only once. |
|   patient.identifier_parameter_value   |   REGEX   |   `^\S+$` |   patient.identifier parameter is a token.  |
|   period_parameter_presence   |   OCCURENCE   | 0..1 |   period parameter can be used only once. |
|   period_parameter_value   |   REGEX   |   `^(eq\|ne\|lt\|gt\|ge\|le\|sa\|eb\|ap)?[0-9]{4}-(0[1-9]\|1[0-2])-(0[1-9]\|1\d\|2[0-8]\|3[01])$` | period parameter is made of a date eventually preceded by a modifier (eq,ne,lt,gt,ge,le,sa,eb or ap).    |
|   related_parameter_presence   |   OCCURRENCE   |   0..1 |   related parameter can be used only once. |
|   related_parameter_value   |   REGEX   |   `^\S+$` |   related parameter references an identifier which is a token.  |
|   security-label_parameter_presence   |   OCCURRENCE   |   0..1 |   security-label parameter can be used only once. |
|   security-label_parameter_value   |   REGEX   |   `^\S+$` |   security-label parameter is a token.  |
|   setting_parameter_presence   |   OCCURRENCE   |   0..1 |   setting parameter can be used only once. |
|   setting_parameter_value   |   REGEX   |   `^\S+$` |   setting parameter is a token.  |
|   status_parameter_presence   |   OCCURRENCE   |   1..1 |   The Document Consumer shall include search status. |
|   status_parameter_value   |   REGEX   |   `^\S+$` |   status parameter is a token.  |
|   type_parameter_presence   |   OCCURRENCE   |   0..1 |   type parameter can be used only once. |
|   type_parameter_value   |   REGEX   |   `^\S+$` |   type parameter is a token.  |


## ITI-68 Retrieve Document assertions

|   id  |   Check's type |   Value |   Description    |
|---    |---    |---    |---    |
|   MethodChecking   |   FIXEDVALUE   |   GET |   HTTP method shall be  GET  |
|   HTTP1VersionChecking   |   FIXEDVALUE  |   HTTP/1.1 |   The HTTP version shall be HTTP/1.1  |
|   URIRegexChecking   |   REGEX   |   `^(\S)+\/(\S)+$` |   URI path of the request shall be directly followed by the document id.  |
|   HostChecking   |   REGEX   |   `^(http[s]?:\/\/)?[a-zA-Z0-9.\-\/]+(\:(\d)+)?$` |   Host Shall be a server name  |
|   Authorization_parameter_presence   |   OCCURRENCE   |   1..1 |   The Authorization parameter is mandatory  |
|   Authorization_parameter_value   |   REGEX   |   `^Bearer\\ [-a-zA-Z0-9._~+\\/]+=*$` |   Authorization parameter's value SHALL be Bearer, followed by the token.  |
|   traceparent_parameter_presence   |   OCCURRENCE   |   0..1 |   The traceparent parameter is mandatory in the HTTP header|
|   traceparent_parameter_value   |   REGEX   |   `^(?!f{2})([\da-f]{2})-(?!f{32})([\da-f]{32})-(?!f{16})([\da-f]{16})-(?!f{2})([\da-f]{2})$` |   traceparent parameter's value SHALL be as defined in fhir.ch, section [tracecontext](https://fhir.ch/ig/ch-epr-mhealth/tracecontext.html). |

## Behaviour of simulators using this HTTP Validation profile
In case of error response from HTTP Validator, simulators shall sent these HTTP error codes :
|   Assertion's id  |   HTTP error code  |
|---    |---    |
|   GETMethodChecking   | 404 |
|   Authorization_parameter_presence   | 401 |
|   Authorization_parameter_presence   | 401 |
|   All other assertions (default)   | 400 |