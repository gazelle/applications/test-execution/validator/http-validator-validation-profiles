{
  "profileType": "HTTPREQUEST",
  "id": "CH_ITI-67-FindDocumentReferences-GET_MHD_Request",
  "name": "CH_ITI-67-FindDocumentReferences_HTTP_GET_Request",
  "description": "Validation Profile for validating a CH:MHD ITI-67 query.",
  "context": "EPR",
  "assertions": [
    {
      "selector": "request.method",
      "id": "MethodChecking",
      "description": "HTTP method shall be GET or POST",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "CLOSEDLIST",
          "values": [
            "GET",
            "POST"
          ]
        }
      ]
    },
    {
      "selector": "request.version",
      "id": "HTTP1VersionChecking",
      "description": "The HTTP version shall be HTTP/1.1",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "FIXEDVALUE",
          "fixedValue": "HTTP/1.1"
        }
      ]
    },
    {
      "selector": "request.uri.path",
      "id": "URIRegexChecking",
      "description": "URI path of the request shall be followed by the MHD resource",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(\\S)+DocumentReference(\\/_search)?$"
        }
      ]
    },
    {
      "selector": "request.headers('Host').values",
      "id": "HostChecking",
      "description": "Host Shall be a server name.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(http[s]?:\\/\\/)?[a-zA-Z0-9.\\-\\/]+(:(\\d)+)?$"
        }
      ]
    },
    {
      "selector": "request.headers('Authorization').values",
      "id": "Authorization_parameter_presence",
      "description": "The Authorization parameter is mandatory.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 1,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.headers('Authorization').values",
      "id": "Authorization_parameter_value",
      "description": "Authorization parameter's value SHALL be Bearer, followed by the token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^Bearer\\ [-a-zA-Z0-9._~+\\/]+=*$"
        }
      ]
    },
    {
      "selector": "request.headers('traceparent').values",
      "id": "traceparent_parameter_presence",
      "description": "The traceparent parameter is mandatory in the HTTP header.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.headers('traceparent').values",
      "id": "traceparent_parameter_value",
      "description": "traceparent parameter's value SHALL be as defined in fhir.ch, section tracecontext.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.headers('traceparent').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(?!f{2})([\\da-f]{2})-(?!0{32})([\\da-f]{32})-(?!0{16})([\\da-f]{16})-([\\da-f]{2})$"
        }
      ]
    },{
      "selector": "request.headers('Accept').values",
      "id": "Accept_parameter_presence",
      "description": "The Accept parameter is mandatory.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 1,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.headers('Accept').values",
      "id": "Accept_parameter_value",
      "description": "Accept parameter's value SHALL be as defined in https://build.fhir.org/http.html#mime-type.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^application\\/fhir\\+(xml|json|turtle)(;( )?fhirVersion=\\d\\.\\d)?$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('author.given').values",
      "id": "author.given_parameter_presence",
      "description": "author.given parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('author.given').values",
      "id": "author.given_parameter_value",
      "description": "author.given parameter is a string.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.headers('author.given').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^[\\s\\S]+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('author.family').values",
      "id": "author.family_parameter_presence",
      "description": "author.family parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('author.family').values",
      "id": "author.family_parameter_value",
      "description": "author.family parameter is a string.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.headers('author.family').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^[\\s\\S]+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('category').values",
      "id": "category_parameter_presence",
      "description": "category parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('category').values",
      "id": "category_parameter_value",
      "description": "category parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('category').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('creation').values",
      "id": "creation",
      "description": "creation parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('creation').values",
      "id": "creation_parameter_value",
      "description": "creation parameter is a dateTime eventually preceded by a modifier (eq,ne,lt,gt,ge,le,sa,eb or ap).",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('creation').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(eq|ne|lt|gt|ge|le|sa|eb|ap)?([0-9]([0-9]([0-9][1-9]|[1-9]0)|[1-9]00)|[1-9]000)(-(0[1-9]|1[0-2])(-(0[1-9]|[1-2][0-9]|3[0-1])(T([01][0-9]|2[0-3]):[0-5][0-9]:([0-5][0-9]|60)(\\.[0-9]{1,9})?)?)?(Z|(\\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00)?)?)?$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('date').values",
      "id": "date",
      "description": "date parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('date').values",
      "id": "date_parameter_value",
      "description": "date parameter is made of a date eventually preceded by a modifier (eq,ne,lt,gt,ge,le,sa,eb or ap).",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('date').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(eq|ne|lt|gt|ge|le|sa|eb|ap)?[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|1\\d|2[0-9]|3[01])$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('event').values",
      "id": "event_parameter_presence",
      "description": "event parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('event').values",
      "id": "event_parameter_value",
      "description": "event parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('event').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('facility').values",
      "id": "facility_parameter_presence",
      "description": "facility parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('facility').values",
      "id": "facility_parameter_value",
      "description": "facility parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('facility').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('format').values",
      "id": "format_parameter_presence",
      "description": "format parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('format').values",
      "id": "format_parameter_value",
      "description": "format parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('format').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('patient').values",
      "id": "patient_parameter_presence",
      "description": "patient parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('patient').values",
      "id": "patient_parameter_value",
      "description": "patient parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('patient').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('patient.identifier').values",
      "id": "patient.identifier_parameter_presence",
      "description": "patient.identifier parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('patient.identifier').values",
      "id": "patient.identifier_parameter_value",
      "description": "patient.identifier parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('patient.identifier').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('period').values",
      "id": "period",
      "description": "period parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('period').values",
      "id": "period_parameter_value",
      "description": "period parameter is made of a date eventually preceded by a modifier (eq,ne,lt,gt,ge,le,sa,eb or ap).",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('period').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^(eq|ne|lt|gt|ge|le|sa|eb|ap)?[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|1\\d|2[0-9]|3[01])$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('related').values",
      "id": "related_parameter_presence",
      "description": "related parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('related').values",
      "id": "related_parameter_value",
      "description": "related parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('related').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('security-label').values",
      "id": "security-label_parameter_presence",
      "description": "security-label parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('security-label').values",
      "id": "security-label_parameter_value",
      "description": "security-label parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('security-label').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('setting').values",
      "id": "setting_parameter_presence",
      "description": "setting parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('setting').values",
      "id": "setting_parameter_value",
      "description": "setting parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('setting').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('status').values",
      "id": "status_parameter_presence",
      "description": "The Document Consumer shall include search status.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 1,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('status').values",
      "id": "status_parameter_value",
      "description": "status parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('status').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('type').values",
      "id": "type_parameter_presence",
      "description": "type parameter can be used only once.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "checks": [
        {
          "type": "OCCURRENCE",
          "minOccurrence": 0,
          "maxOccurrence": 1
        }
      ]
    },
    {
      "selector": "request.uri.queryParams('type').values",
      "id": "type_parameter_value",
      "description": "type parameter is a token.",
      "requirementPriority": "MANDATORY",
      "checksComposition": "oneOf",
      "applyIf": "request.uri.queryParams('type').values.size() > 0",
      "checks": [
        {
          "type": "REGEX",
          "regex": "^\\S+$"
        }
      ]
    }
  ]
}