*** Settings ***
Library        RequestsLibrary
Library        JSONLibrary
Library        Collections
Library        OperatingSystem
Library        String
Library        base64

*** Variables ***

${path}    EHS\\samples\\CH-mCSD\\ITI-90\\A\ -\ Organization\\CH-mCSD\ -\ 001\ -\ valid\ -\ emptyQuery.txt
${validationProfileId}    CH_ITI-90_Organization_GET_mCSD_Query
${content}    R0VUIENhcmVTZXJ2aWNlc1NlbGVjdGl2ZVN1cHBsaWVyL09yZ2FuaXphdGlvbiBIVFRQLzEuMQpBdXRob3JpemF0aW9uOiBCZWFyZXIgZXlKaGJHY2lPaUpTVXpJMU5pSXNJblI1Y0NJZ09pQWlTbGRVSWl3aWEybGtJaUE2SUNKblZtZDZWV3RSWkV0d1pIVk1ibUY0YlRoS1F5MUpZVGgwVlVkTlYwSjFUVEpaVDJSM2FrdGhjRWR6SW4wLmV5SmxlSEFpT2pFM01EZ3pORGcwT0RVc0ltbGhkQ0k2TVRjd09ETTBPREU0TlN3aVlYVjBhRjkwYVcxbElqb3hOekE0TXpRNE1UUXpMQ0pxZEdraU9pSTVPR1pqWkRGaU55MHdaR0U1TFRSa1pHUXRPRFV3TnkxaE0ySTFZbVUzTm1ZMFptSWlMQ0pwYzNNaU9pSm9kSFJ3Y3pvdkwyVm9aV0ZzZEdoemRXbHpjMlV1YVdobExXVjFjbTl3WlM1dVpYUXZhWFZoTFhOemJ5OXlaV0ZzYlhNdlkyZ3RhWFZoSWl3aVlYVmtJam9pWVdOamIzVnVkQ0lzSW5OMVlpSTZJbUZqTkdaa05qSTJMV05pWkdZdE5HSXpNQzFoT1ROaExUYzBZVE0yWVdReU0yUXdZaUlzSW5SNWNDSTZJa0psWVhKbGNpSXNJbUY2Y0NJNkltTm9MV2wxWVMxamJHbGxiblFpTENKelpYTnphVzl1WDNOMFlYUmxJam9pWXpVeU1URTJOakl0WTJWaE55MDBabUZtTFdGa1pqa3RORE5pTlRFMk1HTmpZalV3SWl3aVlXTnlJam9pTVNJc0ltRnNiRzkzWldRdGIzSnBaMmx1Y3lJNld5SXZLaUpkTENKeVpXRnNiVjloWTJObGMzTWlPbnNpY205c1pYTWlPbHNpWkdWbVlYVnNkQzF5YjJ4bGN5MWphQzFwZFdFaUxDSnZabVpzYVc1bFgyRmpZMlZ6Y3lJc0luVnRZVjloZFhSb2IzSnBlbUYwYVc5dUlsMTlMQ0p5WlhOdmRYSmpaVjloWTJObGMzTWlPbnNpWVdOamIzVnVkQ0k2ZXlKeWIyeGxjeUk2V3lKdFlXNWhaMlV0WVdOamIzVnVkQ0lzSW0xaGJtRm5aUzFoWTJOdmRXNTBMV3hwYm10eklpd2lkbWxsZHkxd2NtOW1hV3hsSWwxOWZTd2ljMk52Y0dVaU9pSndkWEp3YjNObFgyOW1YM1Z6WlQxMWNtNDZiMmxrT2pJdU1UWXVOelUyTGpVdU16QXVNUzR4TWpjdU15NHhNQzQxZkU1UFVrMGdjSEp2Wm1sc1pTQnNZWFZ1WTJnZ1pXMWhhV3dpTENKemFXUWlPaUpqTlRJeE1UWTJNaTFqWldFM0xUUm1ZV1l0WVdSbU9TMDBNMkkxTVRZd1kyTmlOVEFpTENKbGVIUmxibk5wYjI1eklqcDdJbU5vWDJWd2NpSTZleUoxYzJWeVgybGtYM0YxWVd4cFptbGxjaUk2SW5WeWJqcG5jekU2WjJ4dUlpd2lkWE5sY2w5cFpDSTZJamMyTURFd01ESTBOamN6TnpNaWZTd2lZMmhmYVhWaElqcDdJbXhoZFc1amFDSTZJbXhoZFc1amFDSjlMQ0pwYUdWZmFYVmhJanA3SW5CMWNuQnZjMlZmYjJaZmRYTmxJanA3SW1OdlpHVWlPaUpPVDFKTklpd2ljM2x6ZEdWdElqb2lkWEp1T205cFpEb3lMakUyTGpjMU5pNDFMak13TGpFdU1USTNMak11TVRBdU5TSjlmWDBzSW1WdFlXbHNYM1psY21sbWFXVmtJanBtWVd4elpTd2libUZ0WlNJNkluSnlaWGx1YjJ4a2N5QlNaWGx1YjJ4a2N5SXNJbkJ5WldabGNuSmxaRjkxYzJWeWJtRnRaU0k2SW5KeVpYbHViMnhrY3lJc0ltZHBkbVZ1WDI1aGJXVWlPaUp5Y21WNWJtOXNaSE1pTENKbVlXMXBiSGxmYm1GdFpTSTZJbEpsZVc1dmJHUnpJbjAubVQ5WlRHTnFUZnNNQnhveUdMYVFsVXI5UFZjRkl0dmxLbVp4alk0X09BSHFpNG5FODJTNW4wbUU0aXlfNVlMX3NiWXROT0RJMUxfTEg2a2VCVENTYlNPWTVSRWtwX05MbTRRTHpkWDR5bko3WV9uTW9ENUg2VEZxSlRybGxLVko1SUt0WGhXdjliTFFLMzVQTExQZU5NWkg0eWwzQ2hsT2x4MElILXpxSEVQY09WNjlrTjF2QzdfSmpqMHZ4M2QwUkZ2Ul8wc2JmRzNYblNpTzNmcHd3ajJ4WWdSZlprZkpxVHVaNTQwNGo5bnU4VGlQSHRDUnFRX0VQdEVpclVURzFDbFgxRGFoSlJLQi1qd0xIVGh4a0tvb1V4c1JBUVpWaHFheWhMTVY4c3h3TmtlSG5OZW1ocXJlT2tlREVMeG4yWkMtVk9RTGU5V1JzWDM3UWFnTzFnCnRyYWNlcGFyZW50OiAwMC0wYWY3NjUxOTE2Y2Q0M2RkODQ0OGViMjExYzgwMzE5Yy1iN2FkNmI3MTY5MjAzMzMxLTAwCkhvc3Q6IGVoZWFsdGhzdWlzc2UuaWhlLWV1cm9wZS5uZXQK

${API_Base_Endpoint}    https://ehealthsuisse.ihe-europe.net/http-validator/rest
&{headers}    Content-Type=application/json    Authorization=GazelleApiKey ${token}
${body}    {
...        "apiVersion": "0.3.0_SNAPSHOT",
...        "validationServiceName": "HTTP Validator",
...        "validationProfileId": "",
...        "validationItems": 
...        [
...         {
...                 "itemId": "first",
...                 "content": "",
...                 "role": "request",
...                 "location": "localhost"
...         }
...         ]
...     }
${token}    bMrrbYdqyBAmwcpeCz4mWqWTgcLS_xqedOyHIhP3Ci3YibEPUe5boJXAjHk1now6urnJYLoA1KItagWicKq2BIDeTlB1fGjQG_2exCqUarcgUfDUorFQW5e_qoIVoy3FSQaAc6DvrOIlq2KO7CwklpPO5UhKZUpGF9JzLjPGM-I=
 

*** Test Cases***
TC_001_Get_Metadata
    Create Session     API_Testing     ${API_Base_Endpoint}
    ${Get_Response}=    GET On Session    API_Testing    /metadata
    ${statusCode}    Convert To String    ${Get_Response.status_code}
    Should Be Equal    ${statusCode}    200

TC_002_Validate_Samples

    ${sample}=    Get File    ${path}
    ${sample64}=   B 64 Encode    ${sample.encode()}

    ${bodyJson}=    evaluate        json.loads('''${body}''')    json
    ${bodyJson}=    Update Value To Json    ${bodyJson}    $..validationProfileId    ${validationProfileId}
    ${bodyJson}=    Update Value To Json    ${bodyJson}    $..validationItems..content    ${content}
    ${body}=    evaluate    json.dumps(${bodyJson})    json

    Create Session     API_Testing     ${API_Base_Endpoint}    verify=true
    ${Post_Response}=    POST On Session    API_Testing    /validation/validate     data=${body}    headers=${headers}

    ${statusCode}    Convert To String    ${Post_Response.status_code}
    Should Be Equal    ${statusCode}    200



