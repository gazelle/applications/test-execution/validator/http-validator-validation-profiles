---
title: CH:PDQm HTTP Validation profiles' assertions 
subtitle: List of all HTTP rules for CH:PDQm/ITI-78
authors: Vincent HOFMAN
date: 2024-03-26
---

# CH:PDQm Mobile Patient Demographics Query's validation profile

## References
This validation profile's documentation references:
- [FHIR's Datatypes](https://build.fhir.org/datatypes.html)
- [String type's modifiers](https://build.fhir.org/search.html#modifiers)

## ITI-78 Search Query assertions

|   id  |   Check's type |   Value |   Description    |
|---    |---    |---    |---    |
|   GETMethodChecking   |   CLOSEDLIST   |   [GET,POST] |   HTTP method shall be either GET or POST  |
|   HTTP1VersionChecking   |   FIXEDVALUE  |   HTTP/1.1 |   The HTTP version shall be HTTP/1.1  |
|   URIRegexChecking   |   REGEX   |   `^(\S)+Patient$` |   URI path of the request shall be followed by the PDQm resource  |
|   HostChecking   |   REGEX   |   `^(http[s]?:\/\/)?[a-zA-Z0-9.\-\/]+(\:(\d)+)?$` |   Host Shall be a server name  |
|   Authorization_parameter_presence   |   OCCURRENCE   |   1..1 |   The Authorization parameter is mandatory  |
|   Authorization_parameter_value   |   REGEX   |   `^Bearer\\ [-a-zA-Z0-9._~+\\/]+=*$` |   Authorization parameter's value SHALL be Bearer, followed by the token.  |
|   traceparent_parameter_presence   |   OCCURRENCE   |   1..1 |   The traceparent parameter is mandatory in the HTTP header|
|   traceparent_parameter_value   |   REGEX   |   `^(?!f{2})([\da-f]{2})-(?!0{32})([\da-f]{32})-(?!0{16})([\da-f]{16})-([\da-f]{2})$` |   traceparent parameter's value SHALL be as defined in fhir.ch, section [tracecontext](https://fhir.ch/ig/ch-epr-mhealth/tracecontext.html). |
|   _id_parameter_presence   |   OCCURRENCE   |   0..1 |   _id parameter can be used only once. |
|   _id_parameter_value   |   REGEX   |   `^\S+$` |   _id parameter is a token.  |
|   active_parameter_presence   |   OCCURRENCE   |   0..1 |   active parameter can be used only once. |
|   active_parameter_value   |   CLOSEDLIST   |   [true,false] |   active parameter is a boolean and accept true or false values.  |
|   family_parameter_presence   |   OCCURRENCE   |   0..1 |   family parameter can be used only once, eventually suffixed by a modifier :contains, :exact or :text. |
|   family_parameter_value   |   REGEX   |  `^[\s\S]+$`   | family parameter is made of a string.    |
|   given_parameter_presence   |   OCCURRENCE   |   0..1 |   given parameter can be used only once, eventually suffixed by a modifier :contains, :exact or :text. |
|   given_parameter_value   |   REGEX   |  `^[\s\S]+$`   | given parameter is made of a string.    |
|   identifier_parameter_presence   |   OCCURRENCE   |   0..1 |   identifier parameter can be used only once. |
|   identifier_parameter_value   |   REGEX   |   `^\S+$` |   identifier parameter is a token.  |
|   telecom_parameter_presence   |   OCCURRENCE   |   0..0 |   The telecom parameter shall not be used.  |
|   birthdate_parameter_presence   |   OCCURRENCE   |   0..1 |   birthdate parameter can be used only once. |
|   birthdate_parameter_value   |   REGEX   |   `^(eq\|ne\|lt\|gt\|ge\|le\|sa\|eb\|ap)?([0-9]([0-9]([0-9][1-9]\|[1-9]0)\|[1-9]00)\|[1-9]000)(-(0[1-9]\|1[0-2])(-(0[1-9]\|[1-2][0-9]\|3[0-1]))?)?$` | birthdate parameter is made of a date eventually preceded by a modifier.    |
|   address_parameter_presence   |   OCCURRENCE   |   0..1 |   address parameter can be used only once, eventually suffixed by a modifier :contains, :exact or :text. |
|   address_parameter_value   |   REGEX   |  `^[\s\S]+$`   | address parameter is made of a string.    |
|   address-city_parameter_presence   |   OCCURRENCE   |   0..1 |   address-city parameter can be used only once, eventually suffixed by a modifier :contains, :exact or :text. |
|   address-city_parameter_value   |   REGEX   |  `^[\s\S]+$`   | address-city parameter is made of a string.    |
|   address-country_parameter_presence   |   OCCURRENCE   |   0..1 |   address-country parameter can be used only once, eventually suffixed by a modifier :contains, :exact or :text. |
|   address-country_parameter_value   |   REGEX   |  `^[\s\S]+$`   | address-country parameter is made of a string.    |
|   address-postalcode_parameter_presence   |   OCCURRENCE   |   0..1 |   address-postalcode parameter can be used only once, eventually suffixed by a modifier :contains, :exact or :text. |
|   address-postalcode_parameter_value   |   REGEX   |  `^[\s\S]+$`   | address-postalcode parameter is made of a string.    |
|   address-state_parameter_presence   |   OCCURRENCE   |   0..1 |   address-state parameter can be used only once, eventually suffixed by a modifier :contains, :exact or :text. |
|   address-state_parameter_value   |   REGEX   |  `^[\s\S]+$`   | address-state parameter is made of a string.    |
|   gender_parameter_presence   |   OCCURRENCE   |   0..1 |   gender parameter can be used only once. |
|   gender_parameter_value   |   CLOSEDLIST   |   [male,Male,female,Female,other,Other,unknown,Unknown] |   active parameter is a token and accept the code or the display values.  |
|   mothersMaidenName_parameter_presence   |   OCCURRENCE   |   0..1 |   mothersMaidenName parameter can be used only once. |
|   mothersMaidenName_parameter_value   |   REGEX   |  `^[\s\S]+$` |   mothersMaidenName parameter is made of a string. |
|   _format_parameter_presence   |   OCCURRENCE   |   0..1 |   _format parameter can be used only once. |
|   _format_parameter_value   |   CLOSEDLIST   |   [json,xml] |   _format parameter's value shall be either json or xml. |

## ITI-78 Retrieve query assertions

|   id  |   Check's type |   Value |   Description    |
|---    |---    |---    |---    |
|   GETMethodChecking   |   FIXEDVALUE   |   GET |   HTTP method shall be  GET  |
|   HTTP1VersionChecking   |   FIXEDVALUE  |   HTTP/1.1 |   The HTTP version shall be HTTP/1.1  |
|   URIRegexChecking   |   REGEX   |   `^(\S)+\/Patient\/(\S)+$` |   URI path of the request shall be followed by the PDQm resource  |
|   HostChecking   |   REGEX   |   `^(http[s]?:\/\/)?[a-zA-Z0-9.\-\/]+(\:(\d)+)?$` |   Host Shall be a server name  |
|   Authorization_parameter_presence   |   OCCURRENCE   |   1..1 |   The Authorization parameter is mandatory  |
|   Authorization_parameter_value   |   REGEX   |   `^Bearer\\ [-a-zA-Z0-9._~+\\/]+=*$` |   Authorization parameter's value SHALL be Bearer, followed by the token.  |
|   traceparent_parameter_presence   |   OCCURRENCE   |   1..1 |   The traceparent parameter is mandatory in the HTTP header|
|   traceparent_parameter_value   |   REGEX   |   `^(?!f{2})([\da-f]{2})-(?!f{32})([\da-f]{32})-(?!f{16})([\da-f]{16})-(?!f{2})([\da-f]{2})$` |   traceparent parameter's value SHALL be as defined in fhir.ch, section [tracecontext](https://fhir.ch/ig/ch-epr-mhealth/tracecontext.html). |